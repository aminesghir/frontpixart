import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { AddReservationComponent } from './Reservation/add-reservation/add-reservation.component';
import { ListReservationComponent } from './Reservation/list-reservation/list-reservation.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SidebarComponent} from './sidebar/sidebar.component';
import {NotifierOptions, NotifierModule} from 'angular-notifier';
import {HttpClientModule} from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
import {GalleriaModule} from 'primeng';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { AddEmployeComponent } from './Employe/add-employe/add-employe.component';
import { ListEmployeComponent } from './Employe/list-employe/list-employe.component';
import { UpdateReservationComponent } from './Reservation/Update-reservation/update-reservation.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import {AuthenticationService, AuthGuardService, FakeBackendProvider, UserService} from './services';
import { SuperviseurRoleComponent } from './superviseur-role/superviseur-role.component';
import { AddDestinationComponent } from './Destinations/add-destination/add-destination.component';
import { ListDestinationComponent } from './Destinations/list-destination/list-destination.component';
import { FacilitiesRoleComponent } from './facilities-role/facilities-role.component';
import {ExcelServiceService} from './services/excel-service.service';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: false,
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

const materialModules = [
  MatSlideToggleModule,
  MatSortModule,
  CommonModule,
  MatPaginatorModule,
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatCardModule,
  MatSelectModule,
  MatListModule,
  MatSidenavModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatPaginatorModule,
  MatTooltipModule,
  FormsModule,
  ReactiveFormsModule



];


@NgModule({
  declarations: [
    AppComponent,
    AddReservationComponent,
    ListReservationComponent,
    SidebarComponent,
    DashboardComponent,
    AddEmployeComponent,
    ListEmployeComponent,
    UpdateReservationComponent,
    LoginComponent,
    AdminComponent,
    SuperviseurRoleComponent,
    AddDestinationComponent,
    ListDestinationComponent,
    FacilitiesRoleComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NotifierModule.withConfig( customNotifierOptions ),
    HttpClientModule,
    MatSelectModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    GalleriaModule

  ],
  exports: [
    materialModules,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [[AuthenticationService, AuthGuardService, FakeBackendProvider, UserService, ExcelServiceService]],
  bootstrap: [AppComponent]
})
export class AppModule { }
