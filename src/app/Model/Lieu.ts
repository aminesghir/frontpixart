export class Lieu {

  id: string;
  depart: string;
  destination: string;
  prixJour: number;
  prixSoir: number;
}
