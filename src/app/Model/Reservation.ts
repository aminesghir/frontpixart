import { Employe } from './Employe';
import {Lieu} from './Lieu';

export class Reservation {
  id: string;
  destination: Lieu;
  dateReservation: Date;
  employess: Employe[];
  validateurs:Employe[];
  etat: string;
  etat2 :string;
  prix: number;
  bu: string;
  cc: string;

}
