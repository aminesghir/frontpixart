export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  userId: string;
  role: string;
  token?: string;
}
