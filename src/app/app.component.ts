import { Component } from '@angular/core';
import {AuthenticationService} from './services';
import {Router} from '@angular/router';
import {Role} from './Model';
import {AppService} from './services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PixartFront';
  currentUser;

  constructor(private authService: AuthenticationService, private router: Router, private appService: AppService) {
    this.authService.currentUser.subscribe(x => this.currentUser = x);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
  }
  getClasses() {
    const classes = {
      'pinned-sidebar': this.appService.getSidebarStat().isSidebarPinned,
      'toggeled-sidebar': this.appService.getSidebarStat().isSidebarToggeled
    }
    return classes;
  }

  logout() {
    this.authService.logout();
    this.authService.islogged=false;
    this.router.navigate(['/login']);
    this.authService.isSupervisor= false;
    this.authService.isFacilities= false;
  }
  toggleSidebar() {
    this.appService.toggleSidebar();
  }
}

