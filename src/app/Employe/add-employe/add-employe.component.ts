import { Component, OnInit } from '@angular/core';
import { Employe } from 'src/app/Model/Employe';
import { Router } from '@angular/router';
import { EmployeService } from 'src/app/Reservation/employe.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-add-employe',
  templateUrl: './add-employe.component.html',
  styleUrls: ['./add-employe.component.css']
})
export class AddEmployeComponent implements OnInit {
  employes: Observable<Employe[]>;
  employe: Employe = new Employe();
  submitted = false;
  constructor(public employeService: EmployeService,
    private router: Router) { }
  sup : Employe ;
  selectedValue: any ;
  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
   this.employeService.getAllEmploye().subscribe(
      res=>{
        this.employes =res;
      }
    );;

    console.log(this.employes)
  }
  newEmploye(): void {
    this.submitted = false;
    this.employe = new Employe();}
  save() {
    console.log('selectedValue',this.selectedValue)
    this.employeService.createEmploye(this.employe)
      .subscribe(data => console.log(data), error => console.log(error));
   // this.employe = new Employe();
    console.log('employe',this.employe)
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/listEmploye']);
  }


}
