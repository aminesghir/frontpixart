import { Component, OnInit } from '@angular/core';
import { Employe } from 'src/app/Model/Employe';
import { Observable } from 'rxjs';
import { EmployeService } from 'src/app/Reservation/employe.service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import {AuthenticationService} from '../../services';

@Component({
  selector: 'app-list-employe',
  templateUrl: './list-employe.component.html',
  styleUrls: ['./list-employe.component.css']
})
export class ListEmployeComponent implements OnInit {

  employes: Observable<Employe[]>;
  private readonly notifier: NotifierService;

  constructor(private employeService: EmployeService,
    private router: Router, notifierService: NotifierService,private authService: AuthenticationService) {
      this.notifier = notifierService;

    }

  ngOnInit() {

    this.reloadData();
  }
  reloadData() {
    this.employes = this.employeService.getAllEmploye();
    console.log('aa',this.employes)
  }
  deleteEmploye(id: string) {
    this.employeService.deleteEmploye(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  updateEmploye(id: string) {
    this.router.navigate(['update', id]);
  
}

}
