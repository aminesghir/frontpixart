import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Reservation} from '../Model/Reservation';
import {ReservationService} from '../Reservation/reservation.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';
import {AuthenticationService} from '../services';

@Component({
  selector: 'app-superviseur-role',
  templateUrl: './superviseur-role.component.html',
  styleUrls: ['./superviseur-role.component.css']
})
export class SuperviseurRoleComponent implements OnInit {
  id : string;
  reservation: any;
  etat= "enCours"
  reservations: Observable<Reservation[]>;
  list: Reservation[];
  private readonly notifier: NotifierService;
  constructor(private route: ActivatedRoute, public service: ReservationService, private router: Router,notifierService: NotifierService,
              private authService: AuthenticationService) {     this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.reloadData();
    console.log(this.authService.currentUserValue.firstName)
  }
  reservations2 =[]
  valider(i){
    this.reservations2[i].etat="valide"
    this.reservations2[i].etat2="enAttente"
    this.id = this.route.snapshot.params['id'];
    this.service.updateEtat(this.reservations2[i].id, this.reservations2[i])
        .subscribe(data => console.log(data), error => console.log(error));
      console.log('reservation',this.reservation);

    this.service.updateEtat2(this.reservations2[i].id, this.reservations2[i])
      .subscribe(data => console.log(data), error => console.log(error));
    console.log('reservation',this.reservation);


  }

  refuser(i){
    this.reservations2[i].etat="refuse"
    this.id = this.route.snapshot.params['id'];
    this.service.updateEtat(this.reservations2[i].id, this.reservations2[i])
      .subscribe(data => console.log(data), error => console.log(error));
    console.log('reservation',this.reservation);

  }
  reloadData() {
    this.service.updateEtat(this.id, this.reservation);
    this.service.updateEtat2(this.id, this.reservation)
    this.service.getAllReservation().subscribe(
      res => {
        console.log(res);
        this.reservations =res;

        for ( let i=0;i<=res.length;i++){
          console.log('from rest',res[i].employess[0].superviseur.id)
          console.log('from service',this.authService.currentUserValue.userId)
          if(res[i].employess[0].superviseur.id ===this.authService.currentUserValue.userId){

            //  this.reservations.push(res[i])
            this.reservations2.push(res[i])
            //  res.splice(i,1)
            // console.log((res))

          }
        }

        //  console.log((this.reservations))
        // console.log('rest after splice',res)

      });
  }


}
