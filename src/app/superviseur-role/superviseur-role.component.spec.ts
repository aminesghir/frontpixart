import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperviseurRoleComponent } from './superviseur-role.component';

describe('SuperviseurRoleComponent', () => {
  let component: SuperviseurRoleComponent;
  let fixture: ComponentFixture<SuperviseurRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperviseurRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperviseurRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
