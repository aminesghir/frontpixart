import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddReservationComponent} from './Reservation/add-reservation/add-reservation.component';
import {DashboardComponent} from './dashboard/dashboard/dashboard.component';
import {ListReservationComponent} from './Reservation/list-reservation/list-reservation.component';
import { ListEmployeComponent } from './Employe/list-employe/list-employe.component';
import { AddEmployeComponent } from './Employe/add-employe/add-employe.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UpdateReservationComponent} from './Reservation/update-reservation/update-reservation.component';
import {LoginComponent} from './login/login.component';
import {AuthGuardService} from './services';
import {Role} from './Model';
import {SuperviseurRoleComponent} from './superviseur-role/superviseur-role.component';
import {AddDestinationComponent} from './Destinations/add-destination/add-destination.component';
import {ListDestinationComponent} from './Destinations/list-destination/list-destination.component';
import {FacilitiesRoleComponent} from './facilities-role/facilities-role.component';
const routes: Routes = [
  {path: '' , component: DashboardComponent, canActivate: [AuthGuardService]},
  {path: 'listReservation' , component: ListReservationComponent,canActivate: [AuthGuardService],
  },
  {path: 'addReservation' , component: AddReservationComponent,    canActivate: [AuthGuardService]
  },
  {path: 'addDestination' , component: AddDestinationComponent,    canActivate: [AuthGuardService]
  },
  {path: 'listDestination' , component: ListDestinationComponent,    canActivate: [AuthGuardService]
  },
  {path: 'listEmploye' , component: ListEmployeComponent,    canActivate: [AuthGuardService], data: {roles: [Role.Facilities]}
  },
  {path: 'addEmploye' , component: AddEmployeComponent,    canActivate: [AuthGuardService],data: {roles: [Role.Facilities]}
  },
  {path: 'login' , component: LoginComponent},

  {path: 'validerReservation' , component: SuperviseurRoleComponent, canActivate: [AuthGuardService], data: {roles: [Role.Admin]}},

  {path:  'updateReservation' , component: UpdateReservationComponent,    canActivate: [AuthGuardService]
  },
  {path: 'finaliserReservation' , component: FacilitiesRoleComponent, canActivate: [AuthGuardService], data: {roles: [Role.Facilities]}}



];
@NgModule({
  imports: [RouterModule.forRoot(routes),
  FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
