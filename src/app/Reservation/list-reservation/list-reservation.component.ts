import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Reservation} from '../../Model/Reservation';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ReservationService} from '../reservation.service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Employe} from '../../Model/Employe';
import {NotifierService} from 'angular-notifier';
import {EmployeService} from '../employe.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {AuthenticationService} from '../../services';
import {User} from '../../Model';
import * as jsPDF from 'jspdf'
import {ExcelServiceService} from '../../services/excel-service.service';
import {Lieu} from '../../Model/Lieu';

@Component({
  selector: 'app-list-reservation',
  templateUrl: './list-reservation.component.html',
  styleUrls: ['./list-reservation.component.css']
})
export class ListReservationComponent implements OnInit {
  etat= "enCours";
  reservations: Reservation [];
  list: Reservation[];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: string[] = ['employe', 'lieuDepart', 'lieuDestination', 'dateReservation' , 'validiteur'];
  dataSource: MatTableDataSource<Reservation>;
  private readonly notifier: NotifierService;
  reservationForm: FormGroup;
  employes:Employe[] ;
  selectedEmploye: number;
  selectedValiditeur: number;
  key: any;
  i: any;
  j: any;

  // tslint:disable-next-line:max-line-length
  selectedRow: any;
  constructor(public service: ReservationService, private router: Router, public serviceEmploye: EmployeService,
              private fb: FormBuilder, notifierService: NotifierService,private authService: AuthenticationService, private excelService: ExcelServiceService) {
    this.notifier = notifierService;
  }
  currentUser: User
  ngOnInit(): void {
    console.log (this.authService.currentUserValue.role)
    this.reloadData();
  }

  reloadData() {
     this.service.getAllReservation().subscribe(
      res => {

        console.log(res);

        this.reservations =res;
        console.log((this.reservations))
      });
  }
  
  deleteReservation(id: string) {
    this.service.deleteReservation(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  goto(id: string) {
    this.router.navigate(['/updateReservation',{id} ]);

}

  @ViewChild('content') content: ElementRef;
  public downloadPDF() {
    let doc = new jsPDF();
    let specialElementHandlers = {
      '#editor' : function(element, renderer) {
        return true;
      }
    };

    let content = this.content.nativeElement;
    doc.fromHTML(content.innerHTML,15, 15, {
      'width':190,
      'elemnentHandler': specialElementHandlers
    });
    doc.save('test.pdf')
  }

  exportAsXLSX():void {
    this.service.getAllReservation().subscribe(
      res => {

        console.log(res);
        this.reservations =res;
        for ( this.key in this.reservations)
        {{const x : any = res[this.key].destination.depart + '->' + res[this.key].destination.destination ;
          this.reservations[this.key].destination = x ;}
          {

            for (this.i in this.reservations[this.key].employess) {
              const y : any = res[this.key].employess[this.i].nom +' ' + res[this.key].employess[this.i].prenom;
           const z : any = res[this.key].employess[this.i].superviseur.nom + ' ' + res[this.key].employess[this.i].superviseur.prenom;
           // this.reservations[this.key].employess = z;
              this.reservations[this.key].employess = y;
              this.reservations[this.key].validateurs=z;

            }

          }


        }

     this.excelService.exportAsExcelFile(this.reservations , 'sample');
      });

  }

  exportByBu(): void {
    this.service.getReservationsByBu("Pixartprinting").subscribe(
      res => {

        console.log(res);
        this.reservations =res;
        for ( this.key in this.reservations)
        {{const x : any = res[this.key].destination.depart + '->' + res[this.key].destination.destination ;
          this.reservations[this.key].destination = x ;}
          {

            for (this.i in this.reservations[this.key].employess) {
              const y : any = res[this.key].employess[this.i].nom +' ' + res[this.key].employess[this.i].prenom;
              const z : any = res[this.key].employess[this.i].superviseur.nom + ' ' + res[this.key].employess[this.i].superviseur.prenom;
              // this.reservations[this.key].employess = z;
              this.reservations[this.key].employess = y;
              this.reservations[this.key].validateurs=z;

            }

          }


        }

        this.excelService.exportAsExcelFile(this.reservations , 'sample');
      });
  }

  exportByBu2(): void {
    this.service.getReservationsByBu("Exaprint").subscribe(
      res => {

        console.log(res);
        this.reservations =res;
        for ( this.key in this.reservations)
        {{const x : any = res[this.key].destination.depart + '->' + res[this.key].destination.destination ;
          this.reservations[this.key].destination = x ;}
          {

            for (this.i in this.reservations[this.key].employess) {
              const y : any = res[this.key].employess[this.i].nom +' ' + res[this.key].employess[this.i].prenom;
              const z : any = res[this.key].employess[this.i].superviseur.nom + ' ' + res[this.key].employess[this.i].superviseur.prenom;
              // this.reservations[this.key].employess = z;
              this.reservations[this.key].employess = y;
              this.reservations[this.key].validateurs=z;

            }

          }


        }

        this.excelService.exportAsExcelFile(this.reservations , 'sample');
      });
  }


  exportByBuAndCc1() : void {
    this.service.getReservationsByBuAndCc("Pixartprinting", "IT").subscribe(
      res => {

        console.log(res);
        this.reservations =res;
        for ( this.key in this.reservations)
        {{const x : any = res[this.key].destination.depart + '->' + res[this.key].destination.destination ;
          this.reservations[this.key].destination = x ;}
          {

            for (this.i in this.reservations[this.key].employess) {
              const y : any = res[this.key].employess[this.i].nom +' ' + res[this.key].employess[this.i].prenom;
              const z : any = res[this.key].employess[this.i].superviseur.nom + ' ' + res[this.key].employess[this.i].superviseur.prenom;
              // this.reservations[this.key].employess = z;
              this.reservations[this.key].employess = y;
              this.reservations[this.key].validateurs=z;

            }

          }


        }

        this.excelService.exportAsExcelFile(this.reservations , 'sample');
      });
  }

  exportByBuAndCc2() : void {
    this.service.getReservationsByBuAndCc("Pixartprinting", "Care").subscribe(
      res => {

        console.log(res);
        this.reservations =res;
        for ( this.key in this.reservations)
        {{const x : any = res[this.key].destination.depart + '->' + res[this.key].destination.destination ;
          this.reservations[this.key].destination = x ;}
          {

            for (this.i in this.reservations[this.key].employess) {
              const y : any = res[this.key].employess[this.i].nom +' ' + res[this.key].employess[this.i].prenom;
              const z : any = res[this.key].employess[this.i].superviseur.nom + ' ' + res[this.key].employess[this.i].superviseur.prenom;
              // this.reservations[this.key].employess = z;
              this.reservations[this.key].employess = y;
              this.reservations[this.key].validateurs=z;

            }

          }


        }

        this.excelService.exportAsExcelFile(this.reservations , 'sample');
      });
  }
  }

  


