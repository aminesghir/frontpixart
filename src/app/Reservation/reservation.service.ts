import { Injectable } from '@angular/core';
import {Reservation} from '../Model/Reservation';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Employe} from '../Model/Employe';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http: HttpClient) { }

  createReservation(reservation:Reservation): Observable<any> {

    return this.http.post(`http://localhost:8282/reservation/save`, reservation);
  }
  updateReservation(id: string, reservation:Reservation): Observable<any> {
    return this.http.put(`http://localhost:8282/reservation/update/${id}`, reservation);

  }
  deleteReservation(id): Observable<any> {

    return this.http.delete(`http://localhost:8282/reservation/delete/${id}`, {responseType: 'text'});

  }

  getAllReservation(): Observable<any> {
    return this.http.get(`http://localhost:8282/reservation/findall`);
  }
  getReservationByid(id: string) :Observable<any> {
    return this.http.get(`http://localhost:8282/reservation/${id}`);
  }

  updateEtat(id, reservation:Reservation): Observable<any> {
    return this.http.put(`http://localhost:8282/reservation/etat/${id}`, reservation);

  }

  updateEtat2(id, reservation:Reservation): Observable<any> {
    return this.http.put(`http://localhost:8282/reservation/etat2/${id}`, reservation);

  }

  getReservationsByBu(bu : string) : Observable<any> {
    return this.http.get(`http://localhost:8282/reservation/findBy/${bu}`);
  }

  getReservationsByBuAndCc(bu : string, cc: string) : Observable<any> {
    return this.http.get(`http://localhost:8282/reservation/findBy/${bu}/${cc}`);
  }


  }
