import { Component, OnInit } from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ReservationService} from '../reservation.service';
import {Employe} from '../../Model/Employe';
import { Reservation } from 'src/app/Model/Reservation';
import { Observable } from 'rxjs';
import { EmployeService } from '../employe.service';
import {DestinationService} from '../../Destinations/destination.service';
import {Lieu} from '../../Model/Lieu';

@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.component.html',
  styleUrls: ['./add-reservation.component.css']
})
export class AddReservationComponent implements OnInit {

  private readonly notifier: NotifierService;
  selectedEmploye: Employe;
  reservation: Reservation = new Reservation();
  employes:Employe[];
  destinations: Lieu[];
  reservationForm: FormGroup;
  submitted = false;
  constructor(public fb: FormBuilder, public service: ReservationService,
              public router: Router, notifierService: NotifierService,public employeService: EmployeService, public destinationService: DestinationService ) {this.notifier = notifierService; }
  emp : Employe ;
  empl : Employe;
  superviseur: Employe;
  lieu: Lieu;
  onChangeLieu(ev){
    this.lieu=this.destinations[ev.target.selectedIndex];
  }
  onChangeEmploye(ev){
    console.log(this.employes);
    console.log(this.employes)
    this.empl=this.employes[ev.target.selectedIndex];
   /* this.employeService.getSuperviseurByEmp(this.empl.id).subscribe(
      res=>{
        this.superviseur =res;
      }
    );*/
   this.superviseur= this.empl.superviseur
    console.log(this.empl)
    console.log('superviseur',this.superviseur)
  }
  onChange(e){
    console.log(this.employes[e.target.selectedIndex]);
    console.log(this.employes)
    this.emp=this.employes[e.target.selectedIndex];
    console.log(this.emp)
  }
  ngOnInit() {
    this.reloadData();
    this.emp=this.employes[0];
    this.empl= this.employes[0]
    this.lieu=this.destinations[0];
    }
    reloadData() {
       this.employeService.getAllEmploye().subscribe(
        res=>{
          this.employes =res;
        }
      );
       this.destinationService.getAllDestination().subscribe(
         res=>{
           this.destinations =res;
           console.log((this.destinations))
         }
       )
    }
    newReservation(): void {
      this.submitted = false;
      this.reservation = new Reservation();}
      listEmp : Employe[] =[];
      listEmpl: Employe[] =[];
    save() {
    console.log(this.selectedEmploye);
   // this.reservation.employe=this.selectedEmploye;
    console.log(this.reservation);
    this.listEmp.push(this.emp);
    this.reservation.validateurs=this.listEmp;
      this.listEmpl.push(this.empl);
      this.reservation.employess=this.listEmpl;
      this.reservation.etat="enCours";
      this.reservation.destination=this.lieu
        this.service.createReservation(this.reservation)
          .subscribe(data => console.log(data), error => console.log(error));
        //this.reservation = new Reservation();
      console.log(typeof this.reservation.dateReservation)
        this.gotoList();
      
    }

  onSubmit() {
    this.submitted = true;
   // this.newReservation();
    this.save();    
  }

  
  gotoList() {
    console.log(this.reservation)
    this.router.navigate(['/listReservation']);
  }

}
