import { Injectable } from '@angular/core';
import {Employe} from '../Model/Employe';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeService {

  constructor(private http: HttpClient) { }
  createEmploye(employe:Employe): Observable<any> {

    return this.http.post(`http://localhost:8282/employe/save`, employe);
  }
  updateEmploye(id: string, value: any): Observable<any> {
    return this.http.put(`http://localhost:8282/employe/update/${id}`, value);

  }
  deleteEmploye(id): Observable<any> {

    return this.http.delete(`http://localhost:8282/employe/delete/${id}`, {responseType: 'text'});

  }

  getAllEmploye() : Observable<any> {
    return this.http.get(`http://localhost:8282/employe/findall`);

  }
  getEmployeByid(id: string): Observable<any> {
    return this.http.get(`http://localhost:8282/employe/find/${id}`);
  }

  getSuperviseurByEmp(id: string) :Observable<any> {
    return this.http.get(`http://localhost:8282/employe/findBySup/${id}`);
  }
  }

