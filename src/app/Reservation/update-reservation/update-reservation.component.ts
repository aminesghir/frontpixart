import { Component, OnInit } from '@angular/core';
import {Reservation} from '../../Model/Reservation';
import {ActivatedRoute, Router} from '@angular/router';
import {ReservationService} from '../reservation.service';
import {Employe} from '../../Model/Employe';
import {NotifierService} from 'angular-notifier';
import {Observable} from 'rxjs';
import {Lieu} from '../../Model/Lieu';

@Component({
  selector: 'app-update-reservation',
  templateUrl: './update-reservation.component.html',
  styleUrls: ['./update-reservation.component.css']
})
export class UpdateReservationComponent implements OnInit {
  private readonly notifier: NotifierService;

 //  id: string;
 // reservation: Reservation;
 //  employes:Employe[];
 //  empl : Employe;
 //  superviseur: Employe;
 //  destinations: Lieu;

  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router, public reservationService: ReservationService,notifierService: NotifierService) { }
  selectedValue: any ;

  ngOnInit(): void {
    // this.reservation = new Reservation();
    //
    // this.id = this.route.snapshot.params['id'];
    //
    // this.reservationService.getReservationByid(this.id)
    //   .subscribe(data => {
    //     console.log(data)
    //     this.reservation = data;
    //   }, error => console.log(error));
  }

  // updateReservation() {
  //   console.log('selectedValue',this.selectedValue)
  //   this.reservationService.updateReservation(this.id, this.reservation)
  //     .subscribe(data => console.log(data), error => console.log(error));
  //   console.log('reservation',this.reservation)
  //
  //   this.gotoList();
  // }
  // onSubmit() {
  //   this.submitted = true;
  //
  //   this.updateReservation();
  // }
  //
  // gotoList() {
  //   this.router.navigate(['/listReservation']);
  // }

}
