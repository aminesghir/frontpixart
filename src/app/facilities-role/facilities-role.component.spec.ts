import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesRoleComponent } from './facilities-role.component';

describe('FacilitiesRoleComponent', () => {
  let component: FacilitiesRoleComponent;
  let fixture: ComponentFixture<FacilitiesRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
