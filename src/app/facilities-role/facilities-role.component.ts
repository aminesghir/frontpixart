import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Reservation} from '../Model/Reservation';
import {NotifierService} from 'angular-notifier';
import {ActivatedRoute, Router} from '@angular/router';
import {ReservationService} from '../Reservation/reservation.service';
import {AuthenticationService} from '../services';

@Component({
  selector: 'app-facilities-role',
  templateUrl: './facilities-role.component.html',
  styleUrls: ['./facilities-role.component.css']
})
export class FacilitiesRoleComponent implements OnInit {
  id : string;
  reservation: any;
  etat2= "en attente de traitement"
  reservations: Observable<Reservation[]>;
  list: Reservation[];
  private readonly notifier: NotifierService;
  reservations2 =[]
  constructor(private route: ActivatedRoute, public service: ReservationService, private router: Router,notifierService: NotifierService,
              private authService: AuthenticationService) {this.notifier = notifierService; }

  ngOnInit(): void {
    console.log (this.authService.currentUserValue.role)
     this.reloadData();
    console.log(this.authService.currentUserValue.firstName)
  }
  Soumettre(i){
    this.reservations2[i].etat="validé et traité"
    this.reservations2[i].etat2="Traité"
    this.id = this.route.snapshot.params['id'];
    this.service.updateEtat2(this.reservations2[i].id, this.reservations2[i])
      .subscribe(data => console.log(data), error => console.log(error));
    console.log('reservation',this.reservation);

    this.service.updateEtat(this.reservations2[i].id, this.reservations2[i])
      .subscribe(data => console.log(data), error => console.log(error));
    console.log('reservation',this.reservation);
  }
  refuser(i){
    this.reservations2[i].etat2="refuse"
    this.reservations2[i].etat="refusé par facilities "
    this.id = this.route.snapshot.params['id'];
    this.service.updateEtat2(this.reservations2[i].id, this.reservations2[i])
      .subscribe(data => console.log(data), error => console.log(error));
    console.log('reservation',this.reservation);

    this.service.updateEtat(this.reservations2[i].id, this.reservations2[i])
      .subscribe(data => console.log(data), error => console.log(error));
    console.log('reservation',this.reservation);
  }

  reloadData() {
    this.service.updateEtat(this.id, this.reservation);
    this.service.updateEtat2(this.id, this.reservation);
    this.service.getAllReservation().subscribe(
      res => {
        console.log(res);
        this.reservations =res;

        for ( let i=0;i<=res.length;i++){

          if(res[i].etat =="valide"){

            //  this.reservations.push(res[i])
            this.reservations2.push(res[i])
            //  res.splice(i,1)
            // console.log((res))

          }

        }



        //  console.log((this.reservations))
        // console.log('rest after splice',res)

      });
  }

}
