import { Component, OnInit } from '@angular/core';
import {DestinationService} from '../destination.service';
import {Lieu} from '../../Model/Lieu';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-destination',
  templateUrl: './add-destination.component.html',
  styleUrls: ['./add-destination.component.css']
})
export class AddDestinationComponent implements OnInit {
submitted =false;
  constructor(private service: DestinationService, private router: Router) { }

model: Lieu = {
    id:'',
    depart:'',
    destination:'',
    prixJour: null,
    prixSoir: null,
}

  ngOnInit(): void {
  }



  save(){
    if(confirm("voulez-vous vraiment ajouter "+ this.model.depart+"vers"+this.model.destination+"?")){
      this.service.createDestination(this.model).subscribe(
        res => {
          console.log(res);
          location.reload()
        },
        err => {
          console.log(err);
        }
      )
    }
    this.gotoList()
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/listDestination']);
  }

}
