import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Lieu} from '../Model/Lieu';

@Injectable({
  providedIn: 'root'
})
export class DestinationService {

  constructor(private http:HttpClient) { }

  createDestination(lieu:Lieu) :Observable<any> {
    return this.http.post(`http://localhost:8282/destination/save`, lieu);
  }

  deleteDestination(id): Observable<any> {

    return this.http.delete(`http://localhost:8282/destination/delete/${id}`, {responseType: 'text'});

  }

  updateDestination(id: string, value: any): Observable<any> {
    return this.http.put(`http://localhost:8282/destination/update/${id}`, value);

  }

  getAllDestination() : Observable<any> {
    return this.http.get(`http://localhost:8282/destination/findall`);

  }

  getDestinationByid(id: string): Observable<any> {
    return this.http.get(`http://localhost:8282/destination/find/${id}`);
  }
}
