import { Component, OnInit } from '@angular/core';
import {Lieu} from '../../Model/Lieu';
import {DestinationService} from '../destination.service';
import {Observable} from 'rxjs';
import {Employe} from '../../Model/Employe';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-list-destination',
  templateUrl: './list-destination.component.html',
  styleUrls: ['./list-destination.component.css']
})
export class ListDestinationComponent implements OnInit {
  destinations : Observable<Lieu[]>;
  private readonly notifier: NotifierService;

  constructor(private service: DestinationService, notifierService: NotifierService) {  this.notifier = notifierService}

  ngOnInit(){
    this.reloadData();
  }

  reloadData() {
    this.destinations = this.service.getAllDestination();
    console.log('aa',this.destinations)
  }

  deleteDestination(id: string) {
    this.service.deleteDestination(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

}
