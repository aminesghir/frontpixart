import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../services';
import { Role } from '../model';
import {AppService} from '../services/app.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) { }
  isCollapsed = true;

  ngOnInit() {
    this.isloggedin();
this.isFacilities();
this.isSupervisor();

  }
  logout() {
    this.authService.logout();
    this.authService.islogged=false;
    this.router.navigate(['/login']);
    this.authService.isSupervisor= false;
    this.authService.isFacilities= false;
  }
  isSupervisor(){
    return this.authService.isSupervisor;
  }
  isFacilities(){
    return this.authService.isFacilities;
  }
  isloggedin(){
   return this.authService.islogged;
  }


}
