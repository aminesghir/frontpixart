import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {Role, User} from '../model';

@Injectable()
export class AuthenticationService {
  islogged:boolean;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  isSupervisor:boolean;
  isFacilities:boolean;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>('/users/authenticate', {username, password}).pipe(map(user => {
      this.islogged=true;
        console.log("aaaaaaaa");
        console.log(user);
      if(user && user.token) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user as User);

      }
      if (user.role==Role.Admin){
        this.isSupervisor=true;
        console.log(this.isSupervisor)
      }
      //console.log(this.authService.currentUserValue.role)
      if (user.role==Role.Facilities){
        this.isFacilities=true;
        console.log(this.isFacilities)
      }
      return user;
    }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}
